# Link Collections
- https://github.com/meirwah/awesome-incident-response

# Incident Frameworks
- [Incident Response Heirarch of Needs](https://github.com/swannman/ircapabilities)

# Playbooks & IR Planning
- [Syntax-IR Playbooks](https://gitlab.com/syntax-ir/playbooks)
- [NIST Incident Response Plan: Building Your Own IR Process Based on NIST Guidelines](https://www.cynet.com/incident-response/nist-incident-response/)
- [How to Build an Incident Response Playbook](https://swimlane.com/blog/incident-response-playbook)

# Misc Repos
- https://github.com/teamdfir

# Tools
## Commercial 
- https://www.intezer.com/

## Linux Tools
### Memmory
- https://github.com/Sysinternals/ProcDump-for-Linux
- https://github.com/microsoft/avml 
- https://docs.microsoft.com/en-us/security/research/project-freta/
- https://github.com/504ensicsLabs

## Windows Tools
- https://github.com/Countercept/chainsaw

