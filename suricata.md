## General Config
- https://thehackerwhorolls.blogspot.com/2019/10/suricata-cheat-sheet.html

## Log Shipping
- https://austinsonger.medium.com/installing-suricata-and-filebeat-on-centos-and-shipping-suricata-logs-to-elastic-siem-48c7c4d784ee

## Parsing JSON 
- https://suricata.readthedocs.io/en/suricata-6.0.1/output/eve/eve-json-examplesjq.html
- https://www.stamus-networks.com/blog/2015/05/18/looking-at-suricata-json-events-on-command-line



