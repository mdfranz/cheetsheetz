# LLM

## Blogs
- [MosaicML](https://www.mosaicml.com/blog)

## Courses
- [Cohere LLMU](https://docs.cohere.com/docs/llmu) and [YouTube Channel](https://www.youtube.com/@CohereAI)
- [LLM Bootcamp 2023](https://www.youtube.com/playlist?list=PL1T8fO7ArWleyIqOy37OVXsP4hFXymdOZ) 

## Meta
- https://github.com/facebookresearch/llama/

## Repos about LLMs
- https://github.com/Hannibal046/Awesome-LLM
- https://github.com/eugeneyan/open-llms
- https://github.com/karpathy
- https://github.com/simonw/llm - and see [blog](https://simonwillison.net/2023/May/18/cli-tools-for-llms/) 
- https://github.com/the-crypt-keeper/can-ai-code

# LLMOps
- [LLMOps (LLM Bootcamp)](https://www.youtube.com/watch?v=Fquj2u7ay40) - May '23
- [https://medium.com/@iamleonie/understanding-llmops-large-language-model-operations-4253820922](https://medium.com/@iamleonie/understanding-llmops-large-language-model-operations-4253820922)
- [Microsoft Open Sources LMOps: A New Research Initiative to Enable Applications Development with Foundation Models, Part I](https://medium.com/towards-artificial-intelligence/microsoft-open-sources-lmops-a-new-research-initiative-to-enable-applications-development-with-d6d7e7ca2059)

## AWS
- [Amazon Bedrock](https://aws.amazon.com/bedrock/) and [Titan](https://aws.amazon.com/bedrock/titan/) and 
[Amazon SageMaker Notebook Instances](https://docs.aws.amazon.com/sagemaker/latest/dg/nbi.html) and [Deep Learning Containers](https://docs.aws.amazon.com/deep-learning-containers/latest/devguide/what-is-dlc.html) with [gitlab](https://github.com/aws/deep-learning-containers)
- [Retrieval Augmented Generation](https://docs.aws.amazon.com/sagemaker/latest/dg/jumpstart-foundation-models-customize-rag.html) 
- [Deploy generative AI models from Amazon SageMaker JumpStart using the AWS CDK](https://github.com/aws-samples/generative-ai-sagemaker-cdk-demo)
- [Use Built-in Algorithms with Pre-trained Models in SageMaker Python SDK](https://sagemaker.readthedocs.io/en/stable/overview.html#use-sagemaker-jumpstart-algorithms-with-pretrained-models) and https://github.com/aws/amazon-sagemaker-examples/
- [Use Machine Learning Frameworks, Python, and R with Amazon SageMaker](https://docs.aws.amazon.com/sagemaker/latest/dg/frameworks.html)

Also see [SageMaker](../aws/sagemaker.md)

# OpenAI
- [How I Turned My Company’s Docs into a Searchable Database with OpenAI](https://medium.com/towards-data-science/how-i-turned-my-companys-docs-into-a-searchable-database-with-openai-4f2d34bd8736) - Apr 23

# Langchain
## Intro Blogs
- [Getting Started with LangChain: A Beginner’s Guide to Building LLM-Powered Applications](https://medium.com/towards-data-science/getting-started-with-langchain-a-beginners-guide-to-building-llm-powered-applications-95fc8898732c) - Apr 23
- [Playing with GPT-3, LangChain, and the OpenAI Embeddings API](https://www.shruggingface.com/blog/langchain-cloudflare-qa-agent) - Feb '23
- [LangChain AI Handbook (Pinecone)](https://www.pinecone.io/learn/langchain/)
- [LangChain Indexes: Document Loaders](https://www.davidgentile.net/langchain-indexes-document-loaders/) - May '23 

## Repos
- https://github.com/kyrolabs/awesome-langchain
- https://github.com/hwchase17/langchain
- https://github.com/gkamradt/langchain-tutorials

## Tabular Data & Pandas
- [Langchain's Pandas & CSV Agents: Revolutionizing Data Querying using OpenAI LLMs](https://blog.futuresmart.ai/langchains-pandas-csv-agents-revolutionizing-data-querying-using-openai-llms) - May '23
- [Speak Your Queries: How Langchain Lets You Chat with Your Database](https://dev.to/ngonidzashe/speak-your-queries-how-langchain-lets-you-chat-with-your-database-p62)

## Tools built on LangChain
- [localGPT](https://github.com/PromtEngineer/localGPT)
- [privateGPT](https://github.com/imartinez/privateGPT) - Ask questions to your documents without an internet connection, using the power of LLMs. 100% private, no data leaves your execution environment at any point. You can ingest documents and ask questions without an internet connection

### Self-Hosting (aka "Air Gapped" LLMs) 
- [Privacy-first AI search using LangChain and Elasticsearch](https://www.elastic.co/blog/privacy-first-ai-search-langchain-elasticsearch) - May and see https://github.com/elastic/blog-langchain-elasticsearch
- [CASALIOY](https://github.com/su77ungr/CASALIOY) - The fastest toolkit for air-gapped LLMs
- [LeapFrog AI](https://github.com/defenseunicorns/leapfrogai) - designed to provide AI-as-a-service in egress limited environments. This project aims to bridge the gap between resource-constrained environments and the growing demand for sophisticated AI solutions, by enabling the hosting of APIs that provide AI-related services.

# Other Libraries
- https://github.com/gventuri/pandas-ai
