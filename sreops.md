# Core Works
- [Google SRE Book](https://sre.google/sre-book/table-of-contents/)

# Postmortems
- [Blameless PostMortems and a Just Culture](https://www.etsy.com/codeascraft/blameless-postmortems)

# On Call
## Articles
- [Increment On Call Issue](https://increment.com/on-call/) - April 2017
- [How to build a successful on-call culture: A guide for engineering teams](https://blog.usebutton.com/fostering-a-strong-engineering-on-call-culture) - June 2018 
- [Alice Goldfuss Oncall Handbook](https://github.com/alicegoldfuss/oncall-handbook)

## Videos
- [Mean Time to Sleep](https://www.youtube.com/watch?v=FLqucVb_et0) - Velocity Conference

## Useful Vendor Guides & Articles
- [PagerDuty Best Practices for On Call Teams](https://goingoncall.pagerduty.com/)
- [On-Call and Incident Response: Lessons for Success, the New Relic Way](https://newrelic.com/blog/best-practices/on-call-and-incident-response-new-relic-best-practices)
- [A manager’s guide to improving on-call](https://www.atlassian.com/incident-management/on-call/improving-on-call)

# Monitoring
- [Monitoring Distributed Systems](https://sre.google/sre-book/monitoring-distributed-systems/) - from Google SRE Book

# Alerting
- [An Alerting strategy for the cloud](https://abstraction.blog/2023/06/13/cloud-alerting-strategy)
