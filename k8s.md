# Tutorials

- [A Crash Course on Kubernetes](https://blog.gruntwork.io/a-crash-course-on-kubernetes-a96c3891ad82) - July 2022
- [Learning Kubernetes, the Easy Way](https://medium.com/towards-data-science/learn-kubernetes-the-easy-way-d1cfa460c013) - September 2022


# Packaging 

## Helm
- https://github.com/helm/helm/releases
- https://helm.sh/docs/intro/quickstart/ 
- https://codefresh.io/docs/docs/new-helm/helm-best-practices/
- https://rafay.co/the-kubernetes-current/the-ultimate-guide-to-helm-charts/

## Operator Pattern

- https://codeburst.io/kubernetes-operators-by-example-99a77ea4ac43
- https://digitalis.io/blog/kubernetes/kubernetes-operators-pros-and-cons/
- https://www.bluematador.com/blog/using-helm-for-kubernetes-management-and-configuration
- https://cloudblogs.microsoft.com/opensource/2020/04/02/when-to-use-helm-operators-kubernetes-ops/
- https://www.openshift.com/blog/build-kubernetes-operators-from-helm-charts-in-5-steps
- https://medium.com/operators/operator-pattern-kubernetes-openshift-380ddc6a147c

# Observability & Visualization
- [caretta](https://github.com/groundcover-com/caretta) - Instant K8s service dependency map, right to your Grafana.

# Lite Implementations

See [k3s](k3s/)

## Minikube
- https://fabianlee.org/2019/02/11/kubernetes-running-minikube-locally-on-ubuntu-using-kvm/
- https://medium.com/@nieldw/switching-from-minikube-with-virtualbox-to-kvm-2f742db704c9

## Microk8s

## KIND


# Platforms and PaaS
- [acorn](https://acorn.io/)
